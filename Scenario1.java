class Scenario1{
    void S1a(int n) 
    {
        float sum=0.0; //C1
        float prod =1.0;
        for (int i=1; i<=n; i++) 
        {sum=sum + i;
        prod = prod * i;
        foo(sum, prod);}
    }
    
    void S1b(int n) {
        float sum=0.0; //C1’
        float prod =1.0; //C
        for (int i=1; i<=n; i++){
            sum=sum + i;
            prod = prod * i;
            foo(sum, prod);
        }
    }
    
    void S1c(int n) {
        float sum=0.0; //C1
        float prod =1.0;
        for (int i=1; i<=n; i++) {
            sum=sum + i;
            prod = prod * i;
            foo(sum, prod); 
        }
    }
}